---
layout: page
title: Frequently Asked Questions (FAQs) and errata
date: 2020-09-09 10:28:42 -0500
permalink: /faq/
---

In this page you can find the [Frequently Asked Questions](#faqstart)
and the pending [errata on the image creation scripts](#errata).

## Frequently Asked Questions
<a name="faqstart" />

1. **Can you add package `foo` to the image? I think it's quite
   important!**
   
   Most likely, my answer will be _no_. I am trying for the images to be
   as minimal and close to a base Debian install as possible; while
   most of us do use `console-tools` and `bash-completion` (two of the
   most common requests), it is not always required. Keep in mind the
   generated images should work for the most minimal Raspberry Pi
   model 1A (128MB RAM). And, all in all, it's just an `apt install`
   away for you anyway!
   
   If there is anything you _really_ need and cannot easily get
   done from the provided images, do contact me. But I don't want to
   change the images offered to everybody just because a feature is missing!

2. **How do I get wireless working?**

   Given we are shipping a minimal installation, I don't want to carry
   all the burden of _network-manager_, _wicd_ or the like. The
   easiest way is to create a `/etc/network/interfaces.d/wlan0` file
   with the settings for your network. You will find an example one in
   place -- All lines are commented out, as naturally, I don't have
   the settings for your wireless network.

   Of course, there might be other options you need — If that's the
   case, I suggest you look at the different options mentioned in the
   [WiFi/HowToUse page of the Debian
   Wiki](https://wiki.debian.org/WiFi/HowToUse).

3. **What about supporting the Raspberry Pi 4 family? You know, they
   are _sweet_ machines!**

   Finally (2020.09.09), full support for all RPi4 models is here! \o/
   Enjoy it.

## Errata
<a name="errata" />

1. The images are set to boot the kernel with consoles in `/dev/tty0`
   (keyboard/HDMI) and `/dev/ttyS1` (serial port, configured to 115200
   bits per second). The correct console for the Raspberry pin headers
   is `/dev/ttyAMA0`.
   
   You can fix it by editing `cmdline.txt` in the first (FAT)
   partition, or in `/boot/firmware/cmdline.txt` if your system has
   already booted, and changing `ttyS1` for `ttyAMA1`.
