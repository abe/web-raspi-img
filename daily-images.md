---
layout: page
title: Daily auto-built images
date: 2020-07-30 12:41:45 -0500
permalink: /daily-images/
---

The following images are *autobuilt daily* using the Debian Buster
(10) repositories. They have *not been tested at all*, and be aware
that might break every now and then! ([please let us
know](mailto:gwolf@debian.org) if you think that is the
case). Available files:

### Most useful files

The following are the files you definitively want to get. The image
file is the data itself, and the shasum is useful to verify against
data corruption (most often due to interrupted downloads). Please
refer to the [instructions to flash an image](/how-to-image) for
further instructions.

| Raspberry family | `xz`-compressed (≈250-350MB)             | Uncompressed (1.5GB)                  |
|------------------|------------------------------------------|---------------------------------------|
| 0 and 1          | [Image file](/daily/raspi_0w.img.xz)     | [Image file](/daily/raspi_0w.img)     |
|                  | [SHA256 sums](/daily/raspi_0w.xz.sha256) | [SHA256 sums](/daily/raspi_0w.sha256) |
|------------------|------------------------------------------|---------------------------------------|
| 2                | [Image file](/daily/raspi_2.img.xz)      | [Image file](/daily/raspi_2.img)      |
|                  | [Sha256 sums](/daily/raspi_2.xz.sha256)  | [Sha256 sums](/daily/raspi_2.sha256)  |
|------------------|------------------------------------------|---------------------------------------|
| 3                | [Image file](/daily/raspi_3.img.xz)      | [Image file](/daily/raspi_3.img)      |
|                  | [Sha256 sums](/daily/raspi_3.xz.sha256)  | [Sha256 sums](/daily/raspi_3.sha256)  |
|------------------|------------------------------------------|---------------------------------------|
| 4                | [Image file](/daily/raspi_4.img.xz)      | [Image file](/daily/raspi_4.img)      |
|                  | [Sha256 sums](/daily/raspi_4.xz.sha256)  | [Sha256 sums](/daily/raspi_4.sha256)  |
|------------------|------------------------------------------|---------------------------------------|

### Useful for debugging

In case you have to report something, please include the relevant
files matching the version you are reporting:

- Build and push information:
  - [File information for the built image files](/daily/lastbuild)
  - [Timestamp for the last build push](/daily/lastpush)

| Raspberry family | Tarball for the base system               | YAML spec for `vmdb` build            | Build log                           |
|------------------|-------------------------------------------|---------------------------------------|-------------------------------------|
| 0 and 1          | [raspi_0w.tar.gz](/daily/raspi_0w.tar.gz) | [raspi_0w.yaml](/daily/raspi_0w.yaml) | [raspi_0w.log](/daily/raspi_0w.log) |
|------------------|-------------------------------------------|---------------------------------------|-------------------------------------|
| 2                | [raspi_2.tar.gz](/daily/raspi_2.tar.gz)   | [raspi_2.yaml](/daily/raspi_2.yaml)   | [raspi_2.log](/daily/raspi_2.log)   |
|------------------|-------------------------------------------|---------------------------------------|-------------------------------------|
| 3                | [raspi_3.tar.gz](/daily/raspi_3.tar.gz)   | [raspi_3.yaml](/daily/raspi_3.yaml)   | [raspi_3.log](/daily/raspi_3.log)   |
|------------------|-------------------------------------------|---------------------------------------|-------------------------------------|
| 4                | [raspi_4.tar.gz](/daily/raspi_4.tar.gz)   | [raspi_4.yaml](/daily/raspi_4.yaml)   | [raspi_4.log](/daily/raspi_4.log)   |
|------------------|-------------------------------------------|---------------------------------------|-------------------------------------|

