---
layout: page
title: What are the installed defaults and configuration settings?
date: 2020-04-21 12:10:25 -0500
permalink: /defaults-and-settings/
---
Our aim is produced an image *as close as possible* to a plain, basic
Debian install. That is, we don't provide an image preloaded with a
desktop environment, programming tools, enabled services, nor any
other *niceties*. You get a simple Debian system, *as close as
possible* as we believe you can get to what `d-i` would yield.

Of course, due to the media being different, there *are* some
differences. We will try to list them all here; for further details on
the specific adequation steps, you might be interested in looking at
our [master vmdb2 image builder
spec](https://salsa.debian.org/raspi-team/image-specs/-/blob/master/raspi_master.yaml).

### Default settings

- **Partitioning**: The image ships as a 1500MB media, which means you
  can use it in any device of that size or greater. The first 300MB
  will be set aside as a boot partition. The image will resize to use
  all of the available space on the first boot — If you want it not to
  span the SD's full size, after [flashing it to your SD
  card](/how-to-image), you can use `fdisk` to create a limiting dummy
  partition at the limit you want to set. After the first boot, just
  delete (or use!) that partition; the auto-resizer will not be run
  again.
- **Root password**: Controversial as it is, we decided to ship the
  images with an *empty root password*. That means, you will be able
  to log in as root locally (not through the network) *without
  providing a password*. We believe this matches the most common use
  cases of Raspberries. Of course, you are encouraged to set a root
  password as soon as possible (specially if your device is to have
  any kind of network connectivity!)
- **Installed packages**: Additionally to a bare Debian base install,
  you will find:
  - Necessary packages for this platform to work: `raspi3-firmware`
    (from non-free ☹; renamed to `raspi-firmware` after Buster and in
    `buster-backports`, already installed on the Raspberry Pi 4
    images.)
  - Managing partitions: `parted`, `dosfstools`
  - Network connectivity tools: `wpasupplicant`, `wireless-tools`,
    `ssh`
- **Network configuration**: If your device has a wired network card,
  it will be enabled with DHCP. Wireless cards are _not_ preconfigured
  at this point. We will probably add a way to configure them from the
  files in the boot partition in the future.

### Configurable pre-kernel-load settings

The Raspberry firmware is set to read and apply settings from the
`config.txt` and `cmdline.txt` files in the firmware partition. This
will be the first partition in your image (please refer to [how to
find your SD card's device](/find-your-sd) if you are doing this from
your regular computer), and it is mounted under `/boot/firmware` when
booting from the Raspberry.

We do not aim to document all said settings, please refer to the
Raspberry Pi documentation:

- [config.txt](https://www.raspberrypi.org/documentation/configuration/config-txt/)
  approximately maps to what you would expect on a PC's BIOS/UEFI,
  settings that are applied before Linux is booted.
- [cmdline.txt](https://www.raspberrypi.org/documentation/configuration/cmdline-txt.md)
  sets the Linux boot parameters.

### Configurable system settings

The image is configured to process the `/boot/firmware/sysconf.txt`
file at boot time; this file documents this functionality can be
disabled by issuing:

	systemctl disable rpi-set-sysconf

The currently defined settings are:

- **root_pw**: Set a password for the root user (by default, it allows
  for a passwordless login)
- **hostname**: Set the system hostname. The default hostname is
  `rpi{model}-{builddate}`

In the future, a way to configure wireless networking should be added
to this section.
